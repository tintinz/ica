
var _LANGUAGE 				= 'sv_SE'; //or: en_US, fr_FR, hu_HU
var _HOST_DOMOTICZ		  	= 'http://192.168.1.143:1337';
var _light_width			= 4

/*
IF YOU HAVE A MEDIABOX FROM ZIGGO (HORIZON), COPY SWITCH_HORIZON.PHP ON A WEBSERVER INSIDE YOUR NETWORK AND CHANGE THE IP.
ENTER THE PATH TO THIS FILE BELOW.
*/
var _HOST_ZIGGO_HORIZON	  	= ''; //e.g. http://192.168.1.3/domoticz/switch_horizon.php';
//var _APIKEY_WUNDERGROUND  	= '982753177bd759bd';
var _WEATHER_CITY 			= 'Nykoping';
var _WEATHER_CITYNAME 		= 'Nyköping'; //show a different city name, leave empty if same as _WEATHER_CITY
var _WEATHER_COUNTRY 		= 'SE';
var _USE_AUTO_POSITIONING 	= false; //don't want to configure positions, use auto positioning
var _USE_FAVORITES			= true; //only used when using auto positioning
var _HIDE_SECONDS_IN_CLOCK  = false; //do not show the seconds in the clock
var _HIDE_MEDIAPLAYER_WHEN_OFF = false; //when you have a mediaplayer connected, hide it if nothing is playing
//var _NEWS_RSSFEED			= 'https://www.svt.se/nyheter/rss.xml';
var _USE_FAHRENHEIT			= false;
var _STANDBY_AFTER_MINUTES  = false; //enter amount of minutes like: 5 (5 minutes)
var _SCROLL_NEWS_AFTER 		= 12000; //milliseconds, so 7000 is 7 seconds


// Online Radio Stream Plugin, Note: you must enable  plugin in column section 'streamplayer', see columns[3]['blocks'] example below.
var _STREAMPLAYER_TRACKS  	= [
								{"track":1,"name":"Music FM","file":"http://stream.musicfm.hu:8000/musicfm.mp3"},
								{"track":2,"name":"Radio 1","file":"http://213.181.210.106:8000/high.mp3"},
								{"track":3,"name":"Test FM","file":"http://213.181.210.106:8000/high.mp3"},
							  ]; 

//Buttons or images to open webpages in an iframe, like a news website or weather forecast
var buttons = {}
buttons.buienradar = {width:12, isimage:false, refreshimage:60000, image: 'https://www.yr.no/place/Sweden/S%C3%B6dermanland/Oxel%C3%B6sund/external_box_hour_by_hour.html', url: 'http://www.weeronline.nl/Europa/Zweden/Oxelosund/4125539'}
buttons.radio = {width:12, image: 'img/radio_on.png', title: 'Radio 2', url: 'http://nederland.fm'}
buttons.nunl = {width:12, icon: 'fa-newspaper-o', title: 'Nu.nl', url: 'http://www.nu.nl'}

//http://www.yr.no/place/Sweden/Södermanland/Oxelösund/
//http://api.buienradar.nl/image/1.0/RadarMapNL?w=285&h=256

//CUSTOM POSITIONING:
//defining wich blocks to show, de numbers represents the IDX of the device in Domoticz
//only define if you want to use a custom width instead of the default

//var frames = {}
//frames.weather = {height:500,frameurl:"https://www.yr.no/place/Sweden/S%C3%B6dermanland/Oxel%C3%B6sund/external_box_small.html",width:8}

var blocks = {}

//Column1 --------------------------------------------------------------------------------------------------------------------------------------------
//blocks[1] = {}
//blocks[1]['width'] = _light_width; //1 to 12, remove this line if you want to use the default (4)
//blocks[1]['title'] = 'Living room' //if you want change the name of switch different then domoticz
//blocks[1]['icon'] = 'fa-eye'; //if you want an other icon instead of the default, choose from: http://fontawesome.io/cheatsheet/
//blocks[1]['image'] = 'bulb_off.png'; //if you want to show an image instead if icon, place image in img/ folder

blocks[3] = {} //Hjärtat
blocks[3]['width'] = _light_width;
blocks[3]['title'] = 'Hallen - Hjärta' //if you want change the name of switch different then domoticz

blocks[50] = {} //Sovrummet - Fönstret
blocks[50]['width'] = _light_width;
blocks[50]['title'] = 'Sovrummet - Fönstret' //if you want change the name of switch different then domoticz

blocks[15] = {} //Köket - Fönstret
blocks[15]['width'] = _light_width;
blocks[15]['title'] = 'Köket - Fönstret' //if you want change the name of switch different then domoticz

blocks[18] = {} //Vardagsrummet - Fönstret
blocks[18]['width'] = _light_width;
blocks[18]['title'] = 'Vardagsrummet - Fönstret' //if you want change the name of switch different then domoticz

blocks[11] = {} //Akvariumet
blocks[11]['width'] = _light_width;
blocks[11]['title'] = 'Akvariumet' //if you want change the name of switch different then domoticz

blocks[72] = {} //Sovrummet - Patrik
blocks[72]['width'] = _light_width;
blocks[72]['title'] = 'Sovrummet - Patrik' //if you want change the name of switch different then domoticz

blocks[73] = {} //Sovrummet - Amanda
blocks[73]['width'] = _light_width;
blocks[73]['title'] = 'Sovrummet - Amanda' //if you want change the name of switch different then domoticz

blocks[64] = {} //3D printer
blocks[64]['width'] = _light_width;
blocks[64]['title'] = '3D-Printer' //if you want change the name of switch different then domoticz

blocks[75] = {} //KAffe
blocks[75]['width'] = _light_width;
blocks[75]['title'] = 'Kaffebryggaren' //if you want change the name of switch different then domoticz



blocks['blocktitle_1'] = {}
blocks['blocktitle_1']['type'] = 'blocktitle';
blocks['blocktitle_1']['image'] = 'ica_tumb.png';
blocks['blocktitle_1']['title'] = 'Tåg';
blocks['col1_top1'] = {}
blocks['col1_top1']['type'] = 'blocktitle';
blocks['col1_top1']['icon'] = 'fa-newspaper-o';
blocks['col1_top1']['title'] = 'Nyheter';

//Column 2 --------------------------------------------------------------------------------------------------------------------------------


// --------------------------------------------------------------------------------------------------------------------------------

//Column 3 --------------------------------------------------------------------------------------------------------------------------------
blocks[30] = {} //Ute temp
blocks[30]['width'] = 12;
blocks[30]['switch'] = true;
blocks[30]['title'] = 'Temp ute'; //if you want change the name of switch different then domoticz
blocks[27] = {} //Ute temp
blocks[27]['width'] = 12;
blocks[27]['title'] = 'Tempratur ute - Framsidan'; //if you want change the name of switch different then domoticz
blocks[27]['switch'] = true;
blocks[28] = {} //inne temp
blocks[28]['width'] = 12;
blocks[28]['title'] = 'Tempratur Inne'; //if you want change the name of switch different then domoticz
blocks[28]['switch'] = true;
blocks[33] = {} //Vind
blocks[33]['width'] = 12;
blocks[33]['switch'] = true;
blocks[33]['title'] = 'Vind'; //if you want change the name of switch different then domoticz

//blocks['news_2'] = {}
//blocks['news_2']['width'] = 12;
//blocks['news_2']['feed'] = 'http://crossorigin.me/http://www.svt.se/nyheter/rss.xml';
//blocks['news_2']['title'] = 'SVT';

blocks['news_2'] = {}
blocks['news_2']['width'] = 12;
blocks['news_2']['vehicle'] = 'Train';
//blocks['news_2']['feed'] = 'http://crossorigin.me/http://api.sl.se/api2/realtimedeparturesV4.XML?key=d39e05e76605466b875bb7cfdb9d6a86&siteid=9701&timewindow=50';
blocks['news_2']['feed'] = 'https://cors-anywhere.herokuapp.com/http://api.sl.se/api2/realtimedeparturesV4.XML?key=d39e05e76605466b875bb7cfdb9d6a86&siteid=9701&timewindow=30';
blocks['news_2']['title'] = 'SL';
//https://cors-anywhere.herokuapp.com/
blocks['news_4'] = {}
blocks['news_4']['width'] = 12;
blocks['news_4']['vehicle'] = 'Bus';
//blocks['news_4']['feed'] = 'http://crossorigin.me/http://api.sl.se/api2/realtimedeparturesV4.XML?key=d39e05e76605466b875bb7cfdb9d6a86&siteid=9701&timewindow=50';
blocks['news_4']['feed'] = 'https://cors-anywhere.herokuapp.com/http://api.sl.se/api2/realtimedeparturesV4.XML?key=d39e05e76605466b875bb7cfdb9d6a86&siteid=9701&timewindow=30';
blocks['news_4']['title'] = 'Bussar';

blocks['col2_top'] = {}
blocks['col2_top']['type'] = 'blocktitle';
blocks['col2_top']['title'] = 'Bussar';
blocks['col2_top1'] = {}
blocks['col2_top1']['type'] = 'blocktitle';
blocks['col2_top1']['title'] = 'Nuvarande temperaturer';

var frames = {}
frames.reklam = {refreshiframe:30000,height:700,frameurl:"https://www.resume.se/globalassets/resume/nyheter/2008/04/01/pressbyran-saljer-dn-med-f/dnpress.jpg",width:12}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------

var columns = {}

columns[1] = {}
columns[1]['blocks'] = ['blocktitle_1','news_2'] //remark: idx 144 produces 2 blocks, so you can use: '144_1' and '144_2' (or of course, just 144 if you want one)
columns[1]['width'] = 4;

columns[3] = {}
columns[3]['blocks'] = ['clock',frames.reklam]
//columns[3]['blocks'] = ['clock','sunrise','horizon','streamplayer',buttons.buienradar,buttons.radio,buttons.calendar,buttons.nunl,buttons.nzbget]
columns[3]['width'] = 4;

columns[2] = {}
columns[2]['blocks'] = ['col2_top','news_4']
columns[2]['width'] = 4; 


//ica sll anrop http://api.sl.se/api2/realtimedeparturesV4.XML?key=d39e05e76605466b875bb7cfdb9d6a86&siteid=9701&timewindow=30
//var columns_standby = {}
//columns_standby[1] = {}
//columns_standby[1]['blocks'] = ['clock','currentweather_big','weather']
//columns_standby[1]['width'] = 12;

//if you want to use multiple screens, use the code below:
//var screens = {}
//screens[1] = {}
//screens[1]['background'] = 'bg1.jpg';
//screens[1]['background_morning'] = 'bg_morning.jpg';
//screens[1]['background_noon'] = 'bg_noon.jpg';
//screens[1]['background_afternoon'] = 'bg_afternoon.jpg';
//screens[1]['background_night'] = 'bg_night.jpg';
//screens[1]['columns'] = [1,2,3]

//screens[2] = {}
//screens[2]['background'] = 'bg3.jpg';
//screens[2]['background_morning'] = 'bg_morning.jpg';
//screens[2]['background_noon'] = 'bg_noon.jpg';
//screens[2]['background_afternoon'] = 'bg_afternoon.jpg';
//screens[2]['background_night'] = 'bg_night.jpg';
//screens[2]['columns'] = [4,5,6]

