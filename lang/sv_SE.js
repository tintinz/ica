
var lang = new Object();
lang['state_on'] = 'PÅ';
lang['state_off'] = 'AV';
lang['state_open'] = 'ÖPPEN';
lang['state_closed'] = 'STÄNGD';
lang['state_movement'] = 'RÖRELSE';
lang['state_nomovement'] = 'INGEN RÖRELSE';
lang['state_switch'] = 'Brytare';
lang['state_nosmoke'] = 'Ingen Rök';
lang['state_smoke'] = 'RÖK DETEKTERAD!';

lang['monday'] = 'Måndag';
lang['tuesday'] = 'Tisdag';
lang['wednesday'] = 'Onsdag';
lang['thursday'] = 'Torsdag';
lang['friday'] = 'Fredag';
lang['saturday'] = 'Lördag';
lang['sunday'] = 'Söndag';

lang['graph_last_hours'] = 'senaste timmen';
lang['graph_today'] = 'idag';
lang['graph_last_month'] = 'sebaste månaden';

lang['mediaplayer_nothing_playing'] = 'Inget att spela upp just nu';
lang['energy_usage'] = 'Energi';
lang['energy_usagetoday'] = 'Energi idag';
lang['energy_totals'] = 'Totalt';
lang['energy_total'] = 'totalt';
lang['energy_now'] = 'nu';
lang['energy_today'] = 'idag';
lang['gas_usagetoday'] = 'Gas idag';
lang['temp_toon'] = 'Vardagsrum';
lang['wind'] = 'Vind';
lang['select'] = 'Välj';
lang['notifications_ns'] = 'avviseringar från NS';

lang['direction_N'] = 'north'
lang['direction_NNE'] = 'north-northeast';
lang['direction_NE'] = 'northeast'
lang['direction_ENE'] = 'east-northeast';
lang['direction_E'] = 'east';
lang['direction_ESE'] = 'east-southeast';
lang['direction_SE'] = 'southeast';
lang['direction_SSE'] = 'south-southeast';
lang['direction_S'] = 'south' ;
lang['direction_SSW'] = 'south-southwest';
lang['direction_SW'] = 'south-west';
lang['direction_WSW'] = 'west-southwest';
lang['direction_W'] = 'west';
lang['direction_WNW'] = 'west-northwest';
lang['direction_NW'] = 'northwest';
lang['direction_NNW'] = 'north-northwest';