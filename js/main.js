
if(typeof(_LANGUAGE)=='undefined') var _LANGUAGE='nl_NL';
if(typeof(_USE_FAVORITES)=='undefined') var _USE_FAVORITES=false;
if(typeof(_USE_AUTO_POSITIONING)=='undefined') var _USE_AUTO_POSITIONING=false;
if(typeof(_HIDE_SECONDS_IN_CLOCK)=='undefined') var _HIDE_SECONDS_IN_CLOCK=false;
if(typeof(_HIDE_MEDIAPLAYER_WHEN_OFF)=='undefined') var _HIDE_MEDIAPLAYER_WHEN_OFF=false;
if(typeof(_USE_FAHRENHEIT)=='undefined') var _USE_FAHRENHEIT=false;
if(typeof(_BACKGROUND_IMAGE)=='undefined') var _BACKGROUND_IMAGE='bg2.jpg';
if(typeof(_NEWS_RSSFEED)=='undefined') var _NEWS_RSSFEED			= 'http://www.nu.nl/rss/algemeen';
if(typeof(_STANDBY_AFTER_MINUTES)=='undefined') var _STANDBY_AFTER_MINUTES = 10;
if(typeof(_WEATHER_CITYNAME)=='undefined') var _WEATHER_CITYNAME = '';
if(typeof(_SCROLL_NEWS_AFTER)=='undefined') var _SCROLL_NEWS_AFTER = 6500;
if(typeof(_STREAMPLAYER_TRACKS)=='undefined') var _STREAMPLAYER_TRACKS = {"track":1,"name":"Music FM","file":"http://stream.musicfm.hu:8000/musicfm.mp3"};
if(typeof(_USE_BEAUFORT)=='undefined') var _USE_BEAUFORT = false;
if(typeof(_TRANSLATE_SPEED)=='undefined') var _TRANSLATE_SPEED = false;

var _TEMP_SYMBOL = '°C';
if(_USE_FAHRENHEIT) _TEMP_SYMBOL = '°F';

$.ajax({url: 'vendor/jquery/jquery-ui.js', async: false,dataType: "script"});
$.ajax({url: 'vendor/jquery/touchpunch.js', async: false,dataType: "script"});
$.ajax({url: 'vendor/bootstrap/js/bootstrap.min.js', async: false,dataType: "script"});
$.ajax({url: 'js/functions.js', async: false,dataType: "script"});
		
$.ajax({url: 'CONFIG.js', async: false,dataType: "script"});
$.ajax({url: 'lang/'+_LANGUAGE+'.js', async: false,dataType: "script"});

$.ajax({url: 'vendor/raphael/raphael-min.js', async: false,dataType: "script"});
$.ajax({url: 'vendor/morrisjs/morris.min.js', async: false,dataType: "script"});
$.ajax({url: 'vendor/moment.js', async: false,dataType: "script"});
$.ajax({url: 'vendor/moment-with-locales.js', async: false,dataType: "script"});
//$.ajax({url: 'vendor/nzbget/nzbget.js', async: false,dataType: "script"});
$.ajax({url: 'vendor/jquery.newsTicker.min.js', async: false,dataType: "script"});
$.ajax({url: 'vendor/superslides/jquery.superslides.js', async: false,dataType: "script"});
$.ajax({url: 'vendor/superslides/hammer.min.js', async: false,dataType: "script"});

$.ajax({url: 'js/switches.js', async: false,dataType: "script"});
$.ajax({url: 'js/blocks.js', async: true,dataType: "script"});
$.ajax({url: 'js/graphs.js', async: false,dataType: "script"});
if(typeof(_DEBUG)!=='undefined' && _DEBUG){
	$.ajax({url: 'js/json_vb.js', async: false,dataType: "script"});
	$.ajax({url: 'js/graph_vb.js', async: false,dataType: "script"});
}
$.ajax({url: 'js/custom.js', async: false,dataType: "script"});

var standby=true;
var standbyActive=false;
var standbyTime=0;
var req;
var slide;
var sliding = false;
var defaultcolumns=false;
var allblocks={}
var alldevices={}

var isMobile = false; //initiate as false
// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;


if(typeof(columns)=='undefined') defaultcolumns = true;
var _GRAPHS_LOADED = new Object();
_GRAPHREFRESH = 5;

if(typeof(blocks)=='undefined') var blocks = {}
if(typeof(columns_standby)=='undefined') var columns_standby = {}

if(typeof(screens)=='undefined'){
	var screens = {}
	screens[1] = {}
	screens[1]['background'] = _BACKGROUND_IMAGE;
	screens[1]['columns'] = []
	if(defaultcolumns===false){
		for(c in columns){
			screens[1]['columns'].push(c);
		}
	}
}
$(document).ready(function(){	
	$('body').attr('unselectable','on')
     .css({'-moz-user-select':'-moz-none',
           '-moz-user-select':'none',
           '-o-user-select':'none',
           '-khtml-user-select':'none',
           '-webkit-user-select':'none',
           '-ms-user-select':'none',
           'user-select':'none'
     }).bind('selectstart', function(){ return false; });

	buildScreens();
	var $slides = $('#slides');
	
	Hammer($slides[0]).on("swipeleft", function(e) {
		$slides.data('superslides').animate('next');
	});

	Hammer($slides[0]).on("swiperight", function(e) {
		$slides.data('superslides').animate('prev');
	});

	$slides.superslides({
		hashchange: true/*,
		animation: 'fade'*/
	});
	setTimeout(function(){
		setInterval(function(){ 
			if(_HIDE_SECONDS_IN_CLOCK==true) $('.clock').html(moment().locale(_LANGUAGE.substr(0,2)).format('HH:mm'));
			else $('.clock').html(moment().locale(_LANGUAGE.substr(0,2)).format('HH:mm:ss'));
			$('.date').html(moment().locale(_LANGUAGE.substr(0,2)).format('D MMMM YYYY'));
			$('.weekday').html(moment().locale(_LANGUAGE.substr(0,2)).format('dddd'));
		},1000);
		
		getDevices(); 		
	},750);
	
	setClassByTime();
	setInterval(function(){ 
		setClassByTime();
	},(60000));
	
}); 

function buildStandby(){
	
	if($('.screenstandby').length==0){
		var screenhtml = '<div class="screen screenstandby fullslider slidestandby" style="height:'+$(window).height()+'px"><div class="row"></div></div>';
		$('div.screen').hide();
		$('div#slides').before(screenhtml);	

		for(c in columns_standby){
			$('div.screenstandby .row').append('<div class="col-xs-'+columns_standby[c]['width']+' colstandby'+c+'"></div>');
			for(b in columns_standby[c]['blocks']){
				$('.block_'+columns_standby[c]['blocks'][b]).first().clone().appendTo('.colstandby'+c);
			}
		}
	}
}

function buildScreens(){
	var num=1;

	for(s in screens){
		var screenhtml = '<div class="screen screen'+s+' fullslider slide'+s+'"';
		if(typeof(screens[s]['background'])!=='undefined') screenhtml+='style="background-image:url(\'img/'+screens[s]['background']+'\');"';
		screenhtml+='><div class="row"></div></div>';
		$('div.contents').append(screenhtml);	
		if(objectlength(screens)>1){
			var check = '';
			if(num==1){
				check='checked';
			}
			$('.slider-pagination').before('<input type="radio" name="slider" class="slide-radio'+num+'" '+check+' id="slider_'+num+'">');
			$('.slider-pagination').append('<label for="slider_'+num+'" class="paginate page'+num+'"></label>');
		}
		
		
		if(defaultcolumns===false){
			for(cs in screens[s]['columns']){
				c = screens[s]['columns'][cs];
				if(typeof(columns[c])!=='undefined'){
					$('div.screen'+s+' .row').append('<div class="col-xs-'+columns[c]['width']+' col'+c+'"></div>');
					for(b in columns[c]['blocks']){
						
						var width=12;
						if(typeof(blocks[columns[c]['blocks'][b]])!=='undefined' && typeof(blocks[columns[c]['blocks'][b]]['width'])!=='undefined') width = blocks[columns[c]['blocks'][b]]['width'];

						var blocktype='';
						if(typeof(blocks[columns[c]['blocks'][b]])!=='undefined' && typeof(blocks[columns[c]['blocks'][b]]['type'])!=='undefined') blocktype = blocks[columns[c]['blocks'][b]]['type'];

						if(blocktype=='blocktitle'){
							$('div.screen'+s+' .row .col'+c).append('<div class="col-xs-12 mh titlegroups transbg"><h3>'+blocks[columns[c]['blocks'][b]]['title']+'</h3></div>');
						}
						else if(columns[c]['blocks'][b]=='weather'){
							if(typeof(loadWeatherFull)!=='function') $.ajax({url: 'js/weather.js', async: false,dataType: "script"});
							$('div.screen'+s+' .row .col'+c).append('<div class="block_'+columns[c]['blocks'][b]+' containsweatherfull"></div>');
							if(_APIKEY_WUNDERGROUND!=="" && _WEATHER_CITY!=="") loadWeatherFull(_WEATHER_CITY,_WEATHER_COUNTRY,$('.weatherfull'));
						}
						else if(columns[c]['blocks'][b]=='currentweather' || columns[c]['blocks'][b]=='currentweather_big'){
							if(typeof(loadWeather)!=='function') $.ajax({url: 'js/weather.js', async: false,dataType: "script"});
							var cl = '';
							if(columns[c]['blocks'][b]=='currentweather_big') $('div.screen'+s+' .row .col'+c).append('<div class="mh transbg big block_'+columns[c]['blocks'][b]+' col-xs-'+width+' containsweather"><div class="col-xs-1"><div class="weather" id="weather"></div></div><div class="col-xs-11"><span class="title weatherdegrees" id="weatherdegrees"></span> <span class="weatherloc" id="weatherloc"></span></div></div>');
							else $('div.screen'+s+' .row .col'+c).append('<div class="mh transbg block_'+columns[c]['blocks'][b]+' col-xs-'+width+' containsweather"><div class="col-xs-4"><div class="weather" id="weather"></div></div><div class="col-xs-8"><strong class="title weatherdegrees" id="weatherdegrees"></strong><br /><span class="weatherloc" id="weatherloc"></span></div></div>');
							if(_APIKEY_WUNDERGROUND!=="" && _WEATHER_CITY!=="") loadWeather(_WEATHER_CITY,_WEATHER_COUNTRY);
						}
						else if(columns[c]['blocks'][b]=='train'){
							if(typeof(getTrainInfo)!=='function') $.ajax({url: 'js/ns.js', async: false,dataType: "script"});
							$('div.screen'+s+' .row .col'+c).append('<div class="train"></div>');
							getTrainInfo();
						}
						else if(columns[c]['blocks'][b]=='traffic'){
							if(typeof(getTraffic)!=='function') $.ajax({url: 'js/traffic.js', async: false,dataType: "script"});
							$('div.screen'+s+' .row .col'+c).append('<div class="traffic"></div>');
							getTraffic();
						}
						else if(columns[c]['blocks'][b]=='news'){
							if(typeof(getNews)!=='function') $.ajax({url: 'js/news.js', async: false,dataType: "script"});
							$('div.screen'+s+' .row .col'+c).append('<div class="news"></div>');
							getNews('news',_NEWS_RSSFEED);
						}
						else if(typeof(columns[c]['blocks'][b])=='string' && columns[c]['blocks'][b].substring(0,5)=='news_'){
						if(typeof(getNews)!=='function') $.ajax({url: 'js/news.js', async: false,dataType: "script"});
							$('div.screen'+s+' .row .col'+c).append('<div class="'+columns[c]['blocks'][b]+'"></div>');
							getNews(columns[c]['blocks'][b],blocks[columns[c]['blocks'][b]]['feed']);
						}
						else if(columns[c]['blocks'][b]=='clock'){
							$('div.screen'+s+' .row .col'+c).append('<div class="transbg block_'+columns[c]['blocks'][b]+' col-xs-'+width+' text-center"><h1 class="clock"></h1><h4 class="weekday"></h4><h4 class="date"></h4></div>');
						}
						else if(columns[c]['blocks'][b]=='sunrise'){
							$('div.screen'+s+' .row .col'+c).append('<div class="block_'+columns[c]['blocks'][b]+' col-xs-'+width+' transbg text-center sunriseholder"><em class="wi wi-sunrise"></em><span class="sunrise"></span><em class="wi wi-sunset"></em><span class="sunset"></span></div>');
						}
						else if(typeof(columns[c]['blocks'][b])=='object' && typeof(columns[c]['blocks'][b]['isimage'])!=='undefined'){
							var random = getRandomInt(1,100000);
							$('div.screen'+s+' .row .col'+c).append(loadImage(random,columns[c]['blocks'][b]));
						}
						else if(columns[c]['blocks'][b]=='horizon'){
							var html ='<div class="containshorizon">';
									html+='<div class="col-xs-4 transbg hover text-center" onclick="ziggoRemote(\'E0x07\')">';
										html+='<em class="fa fa-chevron-left fa-small"></em>';
									html+='</div>';
									html+='<div class="col-xs-4 transbg hover text-center" onclick="ziggoRemote(\'E4x00\')">';
										html+='<em class="fa fa-pause fa-small"></em>';
									html+='</div>';
									html+='<div class="col-xs-4 transbg hover text-center" onclick="ziggoRemote(\'E0x06\')">';
										html+='<em class="fa fa-chevron-right fa-small"></em>';
									html+='</div>';
								html+='</div>';
							$('div.screen'+s+' .row .col'+c).append(html);
						}
						else if(columns[c]['blocks'][b]=='streamplayer'){
							var html ='<div class="transbg containsstreamplayer">';
									html+='<div class="col-xs-12 transbg smalltitle" id="npTitle"><h3></h3></div>';
									html+='<audio id="audio1" preload="none"></audio>';
									html+='<div class="col-xs-4 transbg hover text-center" id="btnPrev">';
										html+='<em class="fa fa-chevron-left fa-small"></em>';
									html+='</div>';
									html+='<div class="col-xs-4 transbg hover text-center" id="playStream">';
										html+='<em class="fa fa-play fa-small" id="stateicon"></em>';
									html+='</div>';
									html+='<div class="col-xs-4 transbg hover text-center" id="btnNext">';
										html+='<em class="fa fa-chevron-right fa-small"></em>';
									html+='</div>';
								html+='</div>';
							$('div.screen'+s+' .row .col'+c).append(html);
							

							var supportsAudio = !!document.createElement('audio').canPlayType;
  							if(supportsAudio) {
  							  var index = 0,
  							  playing = false;
  							  tracks = _STREAMPLAYER_TRACKS,
  							  trackCount = tracks.length,
  							  npTitle = $('#npTitle h3'),
  							  audio = $('#audio1').bind('play', function() {
  							  	//$('#npTitle').fadeIn('slow');
  							    $('#stateicon').removeClass('fa fa-play');
  							    $('#stateicon').addClass('fa fa-pause');
  							    playing = true;
  							  }).bind('pause', function() {
  							  	
  							    $('#stateicon').removeClass('fa fa-pause');
  							    $('#stateicon').addClass('fa fa-play');
  							    playing = false;
  							  }).get(0),
  							  btnPrev = $('#btnPrev').click(function() {
  							    if((index - 1) > -1) {
  							      index--;
  							      loadTrack(index);
  							      if(playing) {
  							        audio.play();
  							      }
  							    } else {
  							      index = 0
  							      loadTrack(trackCount-1);
  							      if(playing) {
  							        audio.play();
  							      }
  							    }
  							  }),
  							  btnNext = $('#btnNext').click(function() {
  							  	
  							  	if((index + 1) < trackCount) {
  							      index++;
  							      loadTrack(index);
  							      
  							      if(playing) {
  							      	//console.log("hit the play button"+playing);
  							        audio.play();
  							      }

  							    } else {
  							      index = 0;
  							      loadTrack(index);
  							      if(playing) {
  							        audio.play();
  							      }
  							    }
  							   // $('#npTitle').fadeIn('slow');
  							    //audio.pause();
  							  }),
  							  loadTrack = function(id) {
  							    npTitle.text(tracks[id].name);
  							    index = id;
  							    audio.src = tracks[id].file;
  							  };
  							  loadTrack(index);
  							}
  							
							$( "#playStream" ).click(function() {
							  var myAudio = $("#audio1").get(0);
							  if (myAudio.paused) {
   							   $('#stateicon').removeClass('fa fa-play');
   							   $('#stateicon').addClass('fa fa-pause');
   							   myAudio.play();
   							 } else {
   							   //$('#npTitle').fadeOut('slow');	
   							   $('#stateicon').removeClass('fa fa-pause');
   							   $('#stateicon').addClass('fa fa-play');
   							   myAudio.pause();
   							}
							});
												
						}
						else if(typeof(columns[c]['blocks'][b])=='object'){
							var random = getRandomInt(1,100000);
							if(typeof(columns[c]['blocks'][b]['frameurl'])!=='undefined') $('div.screen'+s+' .row .col'+c).append(loadFrame(random,columns[c]['blocks'][b]));
							else $('div.screen'+s+' .row .col'+c).append(loadButton(b,columns[c]['blocks'][b]));
						}
						else {
							$('div.screen'+s+' .row .col'+c).append('<div class="mh transbg block_'+columns[c]['blocks'][b]+'"></div>');
						}
					}
				}
			}
		}
		else {
			$('body .row').append('<div class="col-xs-5 col1"><div class="auto_switches"></div><div class="auto_dimmers"></div></div>');
			$('body .row').append('<div class="col-xs-5 col2"><div class="block_weather containsweatherfull"></div><div class="auto_media"></div><div class="auto_states"></div></div>');
			$('body .row').append('<div class="col-xs-2 col3"><div class="auto_clock"></div><div class="auto_sunrise"></div><div class="auto_buttons"></div></div>');

			$('.col2').prepend('<div class="mh transbg big block_currentweather_big col-xs-12 containsweather"><div class="col-xs-1"><div class="weather" id="weather"></div></div><div class="col-xs-11"><span class="title weatherdegrees" id="weatherdegrees"></span> <span class="weatherloc" id="weatherloc"></span></div></div>');
			if(_APIKEY_WUNDERGROUND!=="" && _WEATHER_CITY!==""){
				loadWeatherFull(_WEATHER_CITY,_WEATHER_COUNTRY,$('#weatherfull'));
				loadWeather(_WEATHER_CITY,_WEATHER_COUNTRY);
			}

			$('.col3 .auto_clock').html('<div class="transbg block_clock col-xs-12 text-center"><h1 id="clock" class="clock"></h1><h4 id="weekday" class="weekday"></h4><h4 id="date" class="date"></h4></div>');
			$('.col3 .auto_sunrise').html('<div class="block_sunrise col-xs-12 transbg text-center sunriseholder"><em class="wi wi-sunrise"></em><span id="sunrise" class="sunrise"></span><em class="wi wi-sunset"></em><span id="sunset" class="sunset"></span></div>');
			for(b in buttons){
				if(buttons[b].isimage) $('.col3 .auto_buttons').append(loadImage(b,buttons[b]));
				else $('.col3 .auto_buttons').append(loadButton(b,buttons[b]));
			}
		}
		num++;
	}
}

function setClassByTime(){
	if($('input[name="slider"]:checked').length>0){
		
		var d = new Date();
		var n = d.getHours();

		if (n >= 20 || n <=5){
			newClass = 'night';
		}
		else if (n >= 6 && n <= 10) {
			newClass = 'morning';
		}
		else if (n >= 11 && n <= 15) {
			newClass = 'noon';
		}
		else if (n >= 16 && n <=19) {
			newClass = 'afternoon';
		}
		
		for(s in screens){
			if(typeof(screens[s]['background_'+newClass])!=='undefined'){
				$('.screen.screen'+s).css('background-image','url(\'img/'+screens[s]['background_'+newClass]+'\')');
			}
		}
		
		$('body').removeClass('morning noon afternoon night').addClass(newClass);
	}
}

//STANDBY FUNCTION
if(parseFloat(_STANDBY_AFTER_MINUTES)>0){
   setInterval(function(){
      standbyTime+=1000;
   },1000);

   setInterval(function(){
      if(standbyActive!=true){
         if(standbyTime>=((_STANDBY_AFTER_MINUTES*1000)*60)){
            $('body').addClass('standby');
			if(objectlength(columns_standby)>0) buildStandby();
            if(typeof(_STANDBY_CALL_URL)!=='undefined' && _STANDBY_CALL_URL!==''){
               $.get(_STANDBY_CALL_URL);
               standbyActive=true;
				
            }
         }
      }
   },5000);

   
   if(!isMobile){
	$('body').bind('mousemove', function(e){
		standbyTime=0;
		disableStandby();
	});
   }
	
   $('body').bind('touchstart click', function(e){
      standbyTime=0;
      disableStandby();
   });

   function disableStandby(){
     if(standbyActive==true){
         standbyTime=0;
         if(typeof(_END_STANDBY_CALL_URL)!=='undefined' && _END_STANDBY_CALL_URL!==''){
               $.get(_END_STANDBY_CALL_URL);
         }
      }
	  
	  if(objectlength(columns_standby)>0){
		$('div.screen').show();
	  	$('.screenstandby').remove();
	  }
      $('body').removeClass('standby');
      standbyActive=false;
	 
      
   }
}
//END OF STANDBY FUNCTION

function loadButton(b,button){
	var random = getRandomInt(1,100000);
	if($('#button_'+b).length==0){
		var html = '<div class="modal fade" id="button_'+b+'_'+random+'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
		  html+='<div class="modal-dialog">';
			html+='<div class="modal-content">';
			  html+='<div class="modal-header">';
				html+='<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			  html+='</div>';
			  html+='<div class="modal-body">';
				  html+='<iframe data-src="'+button.url+'" width="100%" height="570" frameborder="0" allowtransparency="true"></iframe> '; 
			  html+='</div>';
			html+='</div>';
		  html+='</div>';
		html+='</div>';
		$('body').append(html);
	}
	
	var width = 12;
	if(typeof(button.width)!=='undefined') width=button.width;
	var html='<div class="col-xs-'+width+' hover transbg" data-toggle="modal" data-target="#button_'+b+'_'+random+'" onclick="setSrc(this);">';
		html+='<div class="col-xs-4 col-icon">';
			if(typeof(button.image)!=='undefined') html+='<img class="buttonimg" src="'+button.image+'" />';
			else html+='<em class="fa '+button.icon+' fa-small"></em>';
		html+='</div>';
		html+='<div class="col-xs-8 col-data">';
			html+='<strong class="title">'+button.title+'</strong><br>';
			html+='<span class="state"></span>';
		html+='</div>';
	html+='</div>';
	return html;
}

function loadFrame(f,frame){
	
	var width = 12;
	if(typeof(frame.width)!=='undefined') width=frame.width;
	var html='<div class="col-xs-'+width+' hover transbg imgblock imgblock'+f+'" style="height:'+frame.height+'px;padding:0px !important;">';
		html+='<div class="col-xs-12 col-data" style="padding:0px !important;">';
			html+='<iframe src="'+frame.frameurl+'" style="width:100%;border:0px;height:'+(frame.height-14)+'px;"></iframe>';
		html+='</div>';
	html+='</div>';
	
	var refreshtime = 60000;
	if(typeof(frame.refreshiframe)!=='undefined') refreshtime = frame.refreshiframe;
	setInterval(function(){ 
		reloadFrame(f,frame);
	},refreshtime);
	
	return html;
}

function reloadFrame(i,frame){
	var sep='?';
	
	if(typeof(frame.frameurl)!=='undefined'){
		var img = frame.frameurl;
		if (img.indexOf("?") != -1) var sep='&';

		if (img.indexOf("?") != -1){
			var newimg = img.split(sep+'t=');
			img = newimg;
		}
		img+=sep+'t='+(new Date()).getTime();
	}
	
	$('.imgblock'+i).find('iframe').attr('src',img);
}

function loadImage(i,image){

	if(typeof(image.image)!=='undefined'){
		var img = image.image;
	}
	
	if($('.imgblockopens'+i).length==0 && typeof(image.url)!=='undefined'){
		var html = '<div class="modal fade imgblockopens'+i+'" id="'+i+'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
		  html+='<div class="modal-dialog">';
			html+='<div class="modal-content">';
			  html+='<div class="modal-header">';
				html+='<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			  html+='</div>';
			  html+='<div class="modal-body">';
				  html+='<iframe data-src="'+image.url+'" width="100%" height="570" frameborder="0" allowtransparency="true"></iframe> '; 
			  html+='</div>';
			html+='</div>';
		  html+='</div>';
		html+='</div>';
		$('body').append(html);
	}

	var width = 12;
	if(typeof(image.width)!=='undefined') width=image.width;
	var html='';
	
	if(typeof(image.url)!=='undefined') html+='<div class="col-xs-'+width+' hover transbg imgblock imgblock'+i+'" data-toggle="modal" data-target="#'+i+'" onclick="setSrc(this);">';
	else html+='<div class="col-xs-'+width+' transbg imgblock imgblock'+i+'">';
	html+='<div class="col-xs-12">';

		html+='<img src="'+img+'" style="max-width:100%;" />';

	html+='</div>';
	html+='</div>';
	
	var refreshtime = 60000;
	if(typeof(image.refresh)!=='undefined') refreshtime = image.refresh;
	if(typeof(image.refreshimage)!=='undefined') refreshtime = image.refreshimage;
	setInterval(function(){ 
		reloadImage(i,image,true);
	},refreshtime);
	
	var refreshtime = 60000;
	if(typeof(image.refreshiframe)!=='undefined') refreshtime = image.refreshiframe;
	setInterval(function(){ 
		reloadIframe(i,image,true);
	},refreshtime);
	
	return html;
}

function reloadImage(i,image){
	var sep='?';
	
	if(typeof(image.image)!=='undefined'){
		var img = image.image;
		if (img.indexOf("?") != -1) var sep='&';

		if (img.indexOf("?") != -1){
			var newimg = img.split(sep+'t=');
			img = newimg;
		}
		img+=sep+'t='+(new Date()).getTime();
	}
	
	$('.imgblock'+i).find('img').attr('src',img);
}

function reloadIframe(i,image){
	var sep='?';
	
	if(typeof(image.url)!=='undefined'){
		var url = image.url;
		if (url.indexOf("?") != -1) var sep='&';

		if (url.indexOf("?") != -1){
			var newurl = url.split(sep+'t=');
			url = newurl;
		}
		url+=sep+'t='+(new Date()).getTime();
	}
	
	if(typeof($('.imgblockopens'+i+' iframe').attr('src')!=='undefined')){
	   
		$('.imgblockopens'+i+' iframe').attr('src',url);
	}
}

